﻿using System;

namespace Cadenes
{
    class Cadenes
    {
        /*CREADOR: Yanick Garcia
         *FECHA:30/11/2022
         *METODO: Pidiendo primero dos palabras y convirtiendolas en strings
         es después posible compararlas con un if y un else if que determinarán
         la respuesta que se printe al final, no considerá iguales las minusculas
         y las mayusculas.
         */
        public void SonIguals()
        {
            Console.Write("Por favor escriba:\n- ");
            string palabra1 = Convert.ToString(Console.ReadLine());
            Console.Write("- ");
            string palabra2 = Convert.ToString(Console.ReadLine());
            if (palabra1 == palabra2)
            {
                Console.WriteLine("Són iguales");
            }
            else if(palabra1!=palabra2)
            {
                Console.WriteLine("No són iguales.");
            }

        }
        /*CREADOR:Yanick Garcia
         *FECHA: 30/11/2022
         *METODO: Al igual que en el anterior programa se piden dos letras que pasaremos
         a strings, pero también convertimos todos sus caracteres en mayúscula, haciendo
         esto el programa hará que palabras diferenciadas por una mayúscula sean consideradas
         iguales, también podría hacerse con minusculas.
         */
        public void SonIguals2()
        {
            Console.Write("Por favor escriba:\n- ");
            string palabra1 = Convert.ToString(Console.ReadLine());
            Console.Write("- ");
            string palabra2 = Convert.ToString(Console.ReadLine());
            string mayus1 = palabra1.ToUpper();
            string mayus2 = palabra2.ToUpper();
            mayus1.CompareTo(mayus2);
            if (mayus1.CompareTo(mayus2) == 0) 
            {
                Console.WriteLine("Són iguales.");
            }
            else if (palabra1.CompareTo(palabra2) != 0)
            {
                Console.WriteLine("No són iguales");
            }
        }
        /*CREADOR: Yanick Garcia
         *FECHA: 30/11/2022
         *METODO: Para eliminar las letras de la string que nos dé el usuario
         lo que se ha hecho en este programa es un bucle con un while que acabará
         cuando el contador tenga el valor de 0, de mientras eliminaremos toda letra
         que sea igual a las que introduzcamos.
         */
        public void PurgaDeCaracters()
        {
            char count= '1' ;
            Console.Write("Por favor escriba una palabra: ");
            string palabra = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Ponga las letras que desee eliminar, cuando escriba 0 el programa finalizará");
            while(count != '0')
            {
                Console.Write("- ");
                count = Convert.ToChar(Console.ReadLine());
                palabra = palabra.Replace(count, ' ');
            }Console.WriteLine(palabra);
        }
        /*CREADOR: Yanick Garcia
         *FECHA: 7/12/2022
         *METODO: De forma similar a la anterior es necesario pedir una palabra y a continuación
         guardar una letra que será sustituida, pero en este caso con otro char bajo el nombre de
         cambio guardaremos un nuevo valor que se intercambiará en la palabra.
         */
        public void SubstitueixElCaracter()
        {
            Console.Write("Por favor escriba una palabra: ");
            string palabra = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Ponga la letra que desee cambiar");
            Console.Write("- ");
            char aCambiar  = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Ahora la letra por la que quiera intercambiarla.");
            Console.Write("- ");
            char cambio = Convert.ToChar(Console.ReadLine());
            palabra = palabra.Replace(aCambiar, cambio);
            Console.WriteLine(palabra);
        }
        /*CREADOR:
         *FECHA:
         *METODO:
         */
        public void DistanciadHamming()
        {
            bool accept = false;
            string ADN1 = "";
            string ADN2 = "";
            int count = 0;
            Console.WriteLine("Escriba el largo de las cadenas de ADN");
            Console.Write("- ");
            int sizeADN = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ahora las strings:");
            while (accept == false)
            {
                Console.Write("- ");
                ADN1 = Console.ReadLine();
                if (sizeADN == ADN1.Length)
                {
                    Console.WriteLine("Guardado!");
                    Console.Write("- ");
                    ADN2 = Console.ReadLine();
                    if (sizeADN == ADN2.Length)
                    {
                        accept = true;
                    }
                    else
                    {
                        Console.WriteLine("No vale!");
                    }
                }
                else
                {
                    Console.WriteLine("No vale!");
                }
            }
            for (int i = 0; i < sizeADN; i++)
            {
                ADN1[i].CompareTo(ADN2[i]);
                if (ADN1[i].CompareTo(ADN2[i]) == 0)
                {
                    count +=1;
                }
            }Console.WriteLine($"Hay un total de {count} en distancia de Hammering");
        }
        /*CREADOR:
         *FECHA:
         *METODO:
         */
        public void Parentesis()
        {
            Console.WriteLine("Escriba parentesis:");
            Console.Write("- ");
            string conjParentesis = Console.ReadLine();
            for (int i = 0; i<=0; i++)
            {
                for (int j = 0; j < conjParentesis.Length; j++)
                {

                }
            }
        }
        /*CREADOR:
         *FECHA:
         *METODO:
         */
        public void Palindrom()
        {

        }
        /*CREADOR:
         *FECHA:
         *METODO:
         */
        public void InverteixLesParaules()
        {

        }
        /*CREADOR:
         *FECHA:
         *METODO:
         */
        public void InverteixLesParaules2()
        {

        }
        /*CREADOR:
         *FECHA:
         *METODO:
         */
        public void HolaIAdeu()
        {

        }
        public void Salir()
        {

        }
        public void Menu()
        {
            Console.WriteLine("1-SonIguals");
            Console.WriteLine("2-SonIguals2");
            Console.WriteLine("3-PurgaDeCaracters");
            Console.WriteLine("4-SubstitueixElCaracter");
            Console.WriteLine("5-DistanciadHamming");
            Console.WriteLine("6-Parentesis");
            Console.WriteLine("7-Palindrom");
            Console.WriteLine("8-InverteixLesParaules");
            Console.WriteLine("9-InverteixLesParaules2");
            Console.WriteLine("a-HolaIAdeu");
            Console.WriteLine("0-Salir");
            Console.WriteLine("Elija una opción:");
            var opcio = Console.ReadLine();
            switch (opcio)
            {
                case "1":SonIguals();
                    break;
                case "2":SonIguals2();
                    break;
                case "3":PurgaDeCaracters();
                    break;
                case "4":SubstitueixElCaracter();
                    break;
                case "5":DistanciadHamming();
                    break;
                case "6":Parentesis();
                    break;
                case "7":Palindrom();
                    break;
                case "8":InverteixLesParaules();
                    break;
                case "9":InverteixLesParaules2();
                    break;
                case "a":HolaIAdeu();
                    break;
                case "0":Salir();
                    break;
                default: Console.WriteLine("Opción invalida");
                    break;
            }
        }
        static void Main(string[] args)
        {
            var menu = new Cadenes();
            menu.Menu();
        }
    }
}
